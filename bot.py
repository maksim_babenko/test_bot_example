
import requests
import misc
from yobit import get_btc
from time import sleep


token = misc.token
URL = 'https://api.telegram.org/bot' + str(token) + '/' 
global last_update_id
last_update_id = 0



def get_updates():
    url = URL + 'getupdates'
    response = requests.get(url).json()
    update_id = response['result'][-1]['update_id']
    return response


def get_message():
    data = get_updates()['result'][-1]

    current_update_id = data['update_id']

    global last_update_id
    if last_update_id != current_update_id:

        last_update_id = current_update_id
        
        chat_id = data['message']['chat']['id']
        message_text = data['message']['text']
    
        message = {'chat_id': chat_id,
                'text': message_text}
        return message
    return None



def send_message(chat_id, text='Wait a second, please...'):
    url = URL + f'sendmessage?chat_id={chat_id}&text={text}'
    requests.get(url)



def main():
    
    while True:

        answer = get_message()
        if answer != None:

            chat_id = answer['chat_id']
            text = answer['text']

            if text == '/btc':
                send_message(chat_id, get_btc())
        else:
            continue

        sleep(2)






if __name__ == '__main__':
    main()