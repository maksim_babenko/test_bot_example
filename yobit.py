import requests

url = 'https://yobit.net/api/2/btc_usd/ticker'

def get_btc():
    response = requests.get(url).json()['ticker']['last']

    return str(response ) + ' usd'

